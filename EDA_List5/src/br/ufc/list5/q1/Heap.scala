package br.ufc.list5.q1

class Heap(heapSize: Int) extends IHeap{
  var pos: Int = 0
  var temp: Int = 0
  var heap : Array[Int] = new Array[Int](heapSize)
  
  override def insertElemHeap(value: Int): Unit ={
    if (pos <= heapSize-1){
      heap(pos) = value
      changeHeapUp(pos)
      pos += 1
      
    }else{
      println("Heap cheio")
      
    }    
  }
  
  def changeHeapUp(pos : Int): Unit ={
    var i = pos
    var newPos = pos
    while (i > 0 ){
      var father = (i-1)/2
      if(heap(father) < heap(newPos)){
        temp = heap(newPos)
        heap(newPos) = heap(father)
        heap(father) = temp
      }else{
        i = - 1
      }
      newPos = father
      i = newPos
    }
  }
  
  override def findElement(value : Int): Boolean ={
    heap.find( _ == value).map{
      case some => {
        println("O valor " + value + " foi encontrado no Heap")     
        return true
      }
    }
    println("O valor " + value + " nao foi encontrado")
    return false
  }
  
  override def removeElemHeap() : Unit={
    if (pos > 0){
      var root = heap(0)
      heap(0) = heap(pos-1)
      heap(pos-1) = 0
      pos -= 1
      changeHeapDown()
      println(root + " removido")
    }else{
      println("Heap vazia")
    }
    
  }
  
  def changeHeapDown(): Boolean ={
    var father = 0
    while(2*father+1 < pos){
      var childLeft = (2*father)+1
      var childRight = (2*father)+2
      var child = 0
      
      if(childRight >= pos){
        childRight = childLeft
      }
      if(heap(childLeft) > heap(childRight)){
        child = childLeft
      }else{
        child = childRight
      }
      if(heap(father) < heap(child)){
        var temp = heap(father)
        heap(father) = heap(child)
        heap(child) = temp 
      }else{
        return false
      }
      father = child
    }
  return true
  }
  
  override def changeElemHeap(value: Int, newValue: Int): Unit ={
    if (findElement(value) == true){
      var elem = heap.indexOf(value)
      heap(elem) = newValue
      changeHeapUp(elem)
    }else{
      println("elemento não encontrado no Heap")
    }
    
  }
  
  override def freeHeap(): Unit ={
    heap = null
    pos = 0
    temp = 0
  }
  
  override def printVector () : Unit ={
    print("(")
    for (i <- 0 until(heap.length)){
      print(heap(i) + ", ")  
    }
    print(")")
  }
  
  
}