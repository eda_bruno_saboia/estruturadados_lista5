

package br.ufc.list5.q1

trait IHeap {
  def insertElemHeap(value : Int)
  def findElement(value : Int): Boolean
  def removeElemHeap()
  def changeElemHeap(value: Int, newValue: Int)
  def freeHeap()
  def printVector ()  
}