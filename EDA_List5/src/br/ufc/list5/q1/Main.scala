package br.ufc.list5.q1

object Main {
  def main(args: Array[String]) {
    var heap: Heap = new Heap(10)
    
    heap.insertElemHeap(2)
    heap.insertElemHeap(10)
    heap.insertElemHeap(100)
    heap.insertElemHeap(5)
    heap.insertElemHeap(1)
    heap.insertElemHeap(9)
    heap.insertElemHeap(11)
    heap.insertElemHeap(50)
    heap.printVector()
    heap.findElement(50)
    heap.findElement(13)
    heap.findElement(4)
    heap.findElement(9)
    heap.findElement(12)
    heap.changeElemHeap(5, 4)
    heap.printVector()
    heap.removeElemHeap()
    heap.printVector()
    heap.removeElemHeap()
    heap.printVector()
    heap.removeElemHeap()
    heap.printVector()
    heap.freeHeap()
  }
}